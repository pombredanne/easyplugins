#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'prost'

import easy_plugins

class TestIntegrationEasyPlugins(object):
    def test_single_run(self):
        """
        Structure of the test files:
        .
        ├── _easy_plugins_prefix_a
        │   └── __init__.py
        ├── _easy_plugins_prefix_b.py
        ├── _easy_plugins_prefix_b.pyc
        └── _easy_plugins_prefix_c.jpg
        """
        plugins_dict = {}
        plugins = easy_plugins.EasyPlugins("_easy_plugins_prefix_")

        for name, plugin in plugins:
            plugins_dict[name] = plugin

        assert(len(plugins_dict) == 2)

        assert(plugins_dict["_easy_plugins_prefix_a"].summary == "prefix_a")
        assert(plugins_dict["_easy_plugins_prefix_a"].required_version == 0.1)

        assert(plugins_dict["_easy_plugins_prefix_b"].summary == "prefix_b")
        assert(plugins_dict["_easy_plugins_prefix_b"].required_version == 0.2)

